<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{.Keyword}} - GoShop.SG</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="gs.css" rel="stylesheet">
</head>
<body class="container">
    <div class="row top10">
        <div class="col-xs-4 col-md-2">
            <a href="/"><img src="logo-beta.png" class="img-responsive"/></a>
        </div>
        <form action=/s >
            <div class="col-xs-6 col-md-4">
                <input class="form-control" type="text" placeholder="{{.Keyword}}" name="w">
            </div>
            <button class="btn btn-danger" type="submit"><span class="glyphicon glyphicon-search" /></button>
        </form>
        <a href="/s?w={{.EscapedKeyword}}&pn={{.NextPn}}&sort=1"><span class="glyphicon glyphicon-arrow-up">Price</span></a>
        <a href="/s?w={{.EscapedKeyword}}&pn={{.NextPn}}&sort=2"><span class="glyphicon glyphicon-arrow-down">Price</span></a>
    </div>
    <div class="container top10">
        <div class="row">
        {{range $idx, $item := .Items}}
            {{if next_tr $idx}}</div><div class="row">{{end}}
            <div class="col-xs-3 col-md-3"><a href="{{$item.Url}}" onclick="trackOutboundLink('{{$item.Url}}'); return false;">
            <img width=200 height=200 class="img-responsive" src="{{$item.Image}}" alt="{{$item.Name}}"/><br>
            <span class="hidden-xs">{{$item.Name}}</span><br>
            S${{printf "%.2f" $item.Price}}</a></div>
        {{else}}
        {{.Keyword}} Not Found
        {{end}}
        </div>
    </div>
    <div class="row top10 col-xs-offset-4 col-md-offset-5">
        {{if ge .PrevPn 0}}
            <a href="/s?w={{.EscapedKeyword}}&pn={{.PrevPn}}&sort={{.Sort}}"><span class="glyphicon glyphicon-chevron-left">Prev</span></a>&nbsp;
        {{end}}
        {{if ge .NextPn 0}}
            <a href="/s?w={{.EscapedKeyword}}&pn={{.NextPn}}&sort={{.Sort}}"><span class="glyphicon glyphicon-chevron-right">Next</span></a>
        {{end}}
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-61936370-1', 'auto');
  ga('send', 'pageview');

var trackOutboundLink = function(url) {
   ga('send', 'event', 'outbound', 'click', url, {'hitCallback':
     function () {
     document.location = url;
     }
   });
}
</script>
</body>
</html>
