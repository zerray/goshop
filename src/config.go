package main

import (
	"encoding/xml"
	"io/ioutil"
)

type Config struct {
	XMLName xml.Name `xml:"config"`

	Listen          string
	HttpReadTimeout int

	LogPath           string
	LogLevel          int32
	LogRotateInterval int64
	LogSizeMax        int64
	LogCountMax       int64

	SphinxHost     string
	SphinxPort     int
	SphinxSqlPort  int
	SphinxTimeout  int
	SphinxPoolSize int

	TemplateFile string
}

func (config *Config) FromFile(filename string) (err error) {
	configFile, err := ioutil.ReadFile(filename)
	if err != nil {
		return
	}
	err = xml.Unmarshal(configFile, config)
	return
}
