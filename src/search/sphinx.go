package search

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/yunge/sphinx"
)

const (
	SORT_DEFAULT    = 0
	SORT_PRICE_ASC  = 1
	SORT_PRICE_DESC = 2
)

type SphinxItem struct {
	Id          uint64
	AttrSrcid   uint32
	AttrCatid   uint32
	AttrMtime   uint32
	AttrPrice   uint64
	AttrShipfee uint64
	AttrJson    []byte
	JsonData    map[string]string
}

func (p *SphinxPool) SearchItem(kw string, off, limit, sort, retry int) (items []*SphinxItem, err error) {
	conn := p.getConn()
	if conn == nil {
		err = errors.New("no available sphinx connection")
		return
	}
	defer func() {
		p.putConn(conn)
	}()

	switch sort {
	case SORT_PRICE_ASC:
		conn.SetSortMode(sphinx.SPH_SORT_ATTR_ASC, "attr_price")
	case SORT_PRICE_DESC:
		conn.SetSortMode(sphinx.SPH_SORT_ATTR_DESC, "attr_price")
	}
	conn.SetLimits(off, limit, p.MatchMax, p.CutOff)
	if retry > 0 {
		conn.SetMatchMode(sphinx.SPH_MATCH_ANY)
	}
	conn.SetRankingMode(sphinx.SPH_RANK_EXPR, "bm25f(1.2,0,{name=10,desc=1})")

	var r *sphinx.Result
	for try_cnt := 0; try_cnt < p.RetryMax; try_cnt += 1 {
		r, err = conn.Query(kw, p.IndexName, "")
		if err != nil {
			conn.Close()
			conn.Open()
			continue
		}
		break
	}

	if err != nil {
		return
	}

	if r.Status != sphinx.SEARCHD_OK {
		err = fmt.Errorf("sphinx error %d", r.Status)
		return
	}

	items = make([]*SphinxItem, len(r.Matches))
	for i, m := range r.Matches {
		items[i] = &SphinxItem{}
		items[i].Id = m.DocId
		items[i].AttrSrcid = m.AttrValues[0].(uint32)
		items[i].AttrCatid = m.AttrValues[1].(uint32)
		items[i].AttrMtime = m.AttrValues[2].(uint32)
		items[i].AttrPrice = m.AttrValues[3].(uint64)
		items[i].AttrShipfee = m.AttrValues[4].(uint64)
		items[i].AttrJson = m.AttrValues[5].([]byte)
		items[i].JsonData = make(map[string]string)
		json.Unmarshal(items[i].AttrJson, &items[i].JsonData)
	}
	return
}
