package search

import (
	"errors"
	"time"

	"github.com/yunge/sphinx"
)

type SphinxPool struct {
	opt  *sphinx.Options
	pool chan *sphinx.Client

	ConnTimeout time.Duration
	IndexName   string
	MatchMax    int
	CutOff      int
	RetryMax    int
}

func NewPool(host string, port, sql_port, timeout, poolsize int) (p *SphinxPool, err error) {
	if poolsize < 1 || poolsize > 1024 {
		err = errors.New("invalid poolsize")
		return
	}

	p = &SphinxPool{}
	p.opt = &sphinx.Options{
		Host:    host,
		Port:    port,
		SqlPort: sql_port,
		Timeout: timeout,
	}
	p.pool = make(chan *sphinx.Client, poolsize)

	p.ConnTimeout = 200 * time.Millisecond
	p.IndexName = "rt_item"
	p.MatchMax = 1000
	p.CutOff = 1000
	p.RetryMax = 2

	for i := 0; i < poolsize; i += 1 {
		conn, err := p.newConn()
		if err != nil {
			return nil, err
		}
		p.putConn(conn)
	}
	return
}

func (p *SphinxPool) newConn() (conn *sphinx.Client, err error) {
	conn = sphinx.NewClient(p.opt)
	err = conn.Open()
	return
}

func (p *SphinxPool) putConn(conn *sphinx.Client) {
	p.pool <- conn
}

func (p *SphinxPool) getConn() (conn *sphinx.Client) {
	select {
	case conn = <-p.pool:
		return
	case <-time.After(p.ConnTimeout):
		return nil
	}
}
