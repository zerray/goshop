package main

import (
	"text/template"

	"./search"
)

type Global struct {
	sphinx *search.SphinxPool
	tpl    *template.Template
}

func (g *Global) Init() (err error) {
	g.sphinx, err = search.NewPool(g_cfg.SphinxHost, g_cfg.SphinxPort,
		g_cfg.SphinxSqlPort, g_cfg.SphinxTimeout, g_cfg.SphinxPoolSize)
	if err != nil {
		return
	}

	g.tpl, err = LoadTemplate(g_cfg.TemplateFile)
	return
}
