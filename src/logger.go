package main

import (
	"fmt"
	"log"
	"path"
	"runtime"

	"./logfile"
)

var (
	infoLogger *log.Logger
)

func LoggerInit(filename string, rotateInterval, maxLogFileSize, maxLogFileCount int64) {
	output, err := logfile.Open(filename, rotateInterval, maxLogFileSize, maxLogFileCount)
	if err != nil {
		log.Fatal(err)
	}
	infoLogger = log.New(output, "", log.LstdFlags|log.Lmicroseconds)
}

func addPrefix(prefix string, v []interface{}) []interface{} {
	var v2 []interface{}
	v2 = append(v2, prefix)
	v2 = append(v2, v...)
	return v2
}
func LogVerbose(v ...interface{}) {
	if g_cfg.LogLevel >= 2 {
		infoLogger.Print("[VERBOSE]", fmt.Sprint(v...))
	}
}
func LogVerbosef(format string, v ...interface{}) {
	if g_cfg.LogLevel >= 2 {
		_, file, line, ok := runtime.Caller(1)
		if ok {
			arg := make([]interface{}, 0)
			arg = append(arg, path.Base(file), line)
			arg = append(arg, v...)
			infoLogger.Printf("[VERBOSE][%s:%d]"+format, arg...)
		} else {
			infoLogger.Printf("[VERBOSE]"+format, v...)
		}
	}
}

func LogDetail(v ...interface{}) {
	if g_cfg.LogLevel >= 1 {
		infoLogger.Print("[DETAIL]", fmt.Sprint(v...))
	}
}

func LogDetailf(format string, v ...interface{}) {
	if g_cfg.LogLevel >= 1 {
		_, file, line, ok := runtime.Caller(1)
		if ok {
			arg := make([]interface{}, 0)
			arg = append(arg, path.Base(file), line)
			arg = append(arg, v...)
			infoLogger.Printf("[DETAIL][%s:%d]"+format, arg...)
		} else {
			infoLogger.Printf("[DETAIL]"+format, v...)
		}
	}
}

func LogInfo(v ...interface{}) {
	infoLogger.Print("[INFO]", fmt.Sprint(v...))
}

func LogInfof(format string, v ...interface{}) {
	_, file, line, ok := runtime.Caller(1)
	if ok {
		arg := make([]interface{}, 0)
		arg = append(arg, path.Base(file), line)
		arg = append(arg, v...)
		infoLogger.Printf("[INFO][%s:%d]"+format, arg...)
	} else {
		infoLogger.Printf("[INFO]"+format, v...)
	}
}

func LogWarning(v ...interface{}) {
	infoLogger.Print("[WARNING]", fmt.Sprint(v...))
}
func LogWarningf(format string, v ...interface{}) {
	_, file, line, ok := runtime.Caller(1)
	if ok {
		arg := make([]interface{}, 0)
		arg = append(arg, path.Base(file), line)
		arg = append(arg, v...)
		infoLogger.Printf("[WARNING][%s:%d]"+format, arg...)
	} else {
		infoLogger.Printf("[WARNING]"+format, v...)
	}
}
func LogError(v ...interface{}) {
	infoLogger.Print("[ERROR]", fmt.Sprint(v...))
}
func LogErrorf(format string, v ...interface{}) {
	_, file, line, ok := runtime.Caller(1)
	if ok {
		arg := make([]interface{}, 0)
		arg = append(arg, path.Base(file), line)
		arg = append(arg, v...)
		infoLogger.Printf("[ERROR][%s:%d]"+format, arg...)
	} else {
		infoLogger.Printf("[ERROR]"+format, v...)
	}
}
func LogFatal(v ...interface{}) {
	infoLogger.Fatal("[FATAL]", fmt.Sprint(v...))
}
