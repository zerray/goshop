package main

import (
	"log"
	"net/http"
	"net/url"
	"runtime"
	"strconv"
	"time"
)

var (
	g_cfg    = &Config{}
	g_server = &Global{}
)

const (
	GOTO_PREFIX = "http://goshop.sg/go?url="
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	err := g_cfg.FromFile("./conf/goshop.xml")
	if err != nil {
		log.Fatal(err)
	}

	err = g_server.Init()
	if err != nil {
		log.Fatal(err)
	}

	LoggerInit(g_cfg.LogPath, g_cfg.LogRotateInterval, g_cfg.LogSizeMax, g_cfg.LogCountMax)

	http.HandleFunc("/search", errHandleWrapper(searchMain))
	http.HandleFunc("/rewrite", errHandleWrapper(rewriteMain))

	serv := &http.Server{
		Addr:        g_cfg.Listen,
		ReadTimeout: time.Duration(g_cfg.HttpReadTimeout) * time.Second,
	}
	err = serv.ListenAndServe()
	if err != nil {
		log.Fatal(err)
	}
}

func str2int(str string) int {
	v, err := strconv.ParseInt(str, 10, 64)
	if err != nil {
		return 0
	}
	return int(v)
}

func next_tr(idx int) bool {
	if idx%4 == 0 && idx > 0 {
		return true
	}
	return false
}

func searchMain(w http.ResponseWriter, r *http.Request) {
	kw := r.FormValue("w")
	pn := str2int(r.FormValue("pn"))
	sort := str2int(r.FormValue("sort"))

	tv := &TplVar{}
	tv.Keyword = kw
	tv.EscapedKeyword = url.QueryEscape(kw)
	tv.NextPn = pn + 1
	tv.PrevPn = pn - 1
	tv.Sort = sort
	tv.Items = make([]*TplItem, 0)

	limit := 20
	off := pn * limit
	items, err := g_server.sphinx.SearchItem(kw, off, limit, sort, 0)
	if err == nil && len(items) == 0 {
		items, err = g_server.sphinx.SearchItem(kw, off, limit, sort, 1) // retry
	}
	if err == nil {
		for _, item := range items {
			ti := &TplItem{}
			ti.Url = GOTO_PREFIX + url.QueryEscape(item.JsonData["url"])
			ti.Image = item.JsonData["image"]
			ti.Name = item.JsonData["name"]
			ti.Price = float64(item.AttrPrice) / 100000
			tv.Items = append(tv.Items, ti)
		}
	}
	if len(items) < limit {
		tv.NextPn = -1
	}

	err = tv.Build(w)
	if err != nil {
		panic(err)
	}

	LogInfof("[query:%s][pn:%d][result:%d][sort:%d]", kw, pn, len(items), sort)
}

func rewriteMain(w http.ResponseWriter, r *http.Request) {
	url := r.FormValue("url")
	http.Redirect(w, r, url, http.StatusFound)
	LogInfof("goto[url:%s]", url)
}
