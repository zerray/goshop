package main

import (
	"net/http"
)

func errHandleWrapper(fn http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if e, ok := recover().(error); ok {
				LogWarning(e)
				http.Error(w, e.Error(), 500)
			}
		}()
		fn(w, r)
	}
}
