GO=/usr/local/go/bin/go
GOFLAGS=
SRC=$(shell find src -maxdepth 1 -name "*.go" -type f)

all: goshop

goshop: $(SRC)
	$(GO) $(GOFLAGS) build -o $@ $^
